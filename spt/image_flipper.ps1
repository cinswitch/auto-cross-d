$path = "D:\dev\repos\vn\auto-cross-d\game\images\elliot\Mechanic_Intro" #target directory

[System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")
Get-ChildItem -recurse ($path) -include @("*.png", "*.jpg") |
ForEach-Object {
  $image = [System.Drawing.image]::FromFile( $_ )
  $image.rotateflip("RotateNoneFlipX")
  $image.save($_)
}