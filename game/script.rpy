﻿define sam = Character("Sam")
define elliot = Character("Elliot")
define graham = Character("Graham")

define woman01 = Character("Cheery Woman")
define man01 = Character("Laid Back Man")

define n = nvl_narrator

# note: possible sam layouts:
# poses:
# clasped
# relaxed
# scared
#
# expressions:
# angry
# happy
# neutral
# surprised
# worried


label start:
label day_1_introductions:
    scene bg lobby zoom .6

    show sam mechanic relaxed neutral right with moveinleft:
        xanchor 0.5 yanchor 1.0 ypos 1.3 xpos .2

    # If we wanna go with "it's his first day"

    "*ding-a-ling*"

    "..."

    "There's nobody at the desk..."

    show sam worried with ease:
        xpos .15

    "Do I ring the bell or just go in the back?"

    "Or do I-"

    show elliot mechanicintro neutral neutral at center with dissolve:
        yanchor 1.0 ypos 1.3 xzoom -1

    elliot "What's up, my man?"

    show sam scared surprised:
        linear .1 xpos .1
    # sam jumps

    show sam with ease:
        xanchor 0.5 xpos .1
    sam "Eeee!"

    show elliot surprised happy
    elliot "Whoa, sorry, didn't mean to startle you!"

    show elliot mechanicintro neutral neutral
    elliot "Are you the intern?"

    show sam relaxed worried
    sam "A-Apprentice, and yeah."

    show elliot neutral happy with ease:
        xpos .45
    elliot "Cool, cool, lemme show you around."

    elliot "There's not a lot, but might as well give you the little tour before we open. Sound good with you?"

    show sam clasped neutral
    sam "Sure, sounds good."

    show elliot:
        xzoom 1
    elliot "Cool. Right now, we're in the main lobby. The bell will go off when customers come in, like with you."

    elliot "Usually one of us have to drop what we're doing to come out and deal with them, it's a real pain."

    elliot "One of your responsibilities will be to go greet customers, so we can keep working."

    show sam relaxed happy
    sam "Got it."

    show elliot mechanicintro neutral neutral:
        xpos .9
    with ease

    show sam neutral:
        xpos .5
    with ease

    show elliot:
        xzoom -1

    elliot "All you need to do is look up their info on this computer over here, settle any debts, and grab their keys for em."

    elliot "That said, we aren't hiring you just to work as some receptionist, that'd be a waste!"

    elliot "Let's hit the garage next. It's the door right next to you."

    sam "Ah, ok."

    hide sam neutral with dissolve

    show elliot mechanicintro neutral neutral:
        xpos .6
    with ease

    hide elliot with dissolve

    scene bg garage
    show sam mechanic clasped neutral left:
        xanchor 0.5 yanchor 1.0 xpos .8 ypos 1.1 zoom .7
    show elliot mechanicintro surprised happy at center:
        xanchor 0.5 yanchor 1.0 ypos 1.1 zoom .7 xzoom 1
    with fade

    elliot "This is the garage. Most of the work happens here, like you'd expect."

    show elliot mechanicintro neutral happy

    elliot "We've got everything you'd ever need to maintain or repair anything made in the last couple decades."

    elliot "Really wish we'd get some of this stuff updated, though."

    show sam scared neutral with ease:
        xpos .7

    sam "Whoa, I've never wrenches with heads like these before!"

    elliot "Ha ha, yeah, some manufacturers need custom tools. A lot of those are for real old models. I think they came with the shop."

    show sam with ease:
        xpos .3

    show elliot mechanicintro neutral neutral:
        xzoom -1

    sam "I've never been able to play with a hydraulic jack before, being able to walk under a car is surreal!"

    show elliot mechanicintro surprised neutral

    elliot "Ah, yeah, it's real convenient. I feel like we don't use it as much as we ought to, though."

    elliot "Graham still prefers sliding on his back to get up close and personal."

    show sam relaxed

    elliot "If you want to give it a crack, though, feel free."

    show sam scared happy right
    sam "Maybe with your supervision, I don't want to wreak anything..."

    show elliot mechanicintro neutral happy
    elliot "Don't worry too hard about it. We're insured for a reason."

    elliot "Hmm, we should be opening soon, let's wrap this up. Follow me, there's one more place to hit."

    show elliot:
        xzoom 1

    show elliot with ease:
        xpos .7

    hide elliot with dissolve

    show sam clasped neutral with ease:
        xpos .7

    hide sam with dissolve

    scene bg lobby
    show sam mechanic clasped neutral right at center:
        yanchor 1.0 ypos 1.3
    show elliot mechanicintro neutral neutral at right:
        yanchor 1.0 ypos 1.3 xzoom 1
    with fade

    elliot "Right back here."

    show elliot at offscreenright with moveoutright:
        ypos 1.3
    show sam at offscreenright with moveoutright:
        ypos 1.3

    scene bg closet
    show elliot mechanicintro neutral neutral at left:
        yanchor 1.0 ypos 1.3
    show sam mechanic relaxed neutral left at center:
        xanchor .5 yanchor 1.0 ypos 1.3
    with fade

    elliot "This here's an extra work room. We've been renovating this area a bit lately."

    elliot "Last I heard Graham was trying to turn it into an area where we could do some style work."

    show sam neutral right
    sam "Interesting, I haven't seen many shops around here that tackle that."

    show elliot with ease:
        xpos .1

    elliot "Ah, there he is! Hey Graham! The intern's here!"

    show graham neutral at right behind sam:
        yanchor 1.0 ypos 1.5 xzoom 1 zoom .925

    show graham neutral at right:
        linear .3 yanchor 1.0 ypos 1.3

    with dissolve

    show graham neutral at right:
        yanchor 1.0 ypos 1.3

    graham "Hmm?"

    show graham neutral:
        xzoom -1

    graham "Oh!"

    show graham neutral with ease:
        xpos .8
    graham "Hey! Sam! Nice to meet you, bud! Ready to get some hands-on experience?"

    show sam relaxed happy
    sam "Y-yeah!"

    graham "Nervous? Don't worry, I don't bite!"
    # he kinda comes in a lil to close and does a back pat

    show sam with ease:
        xpos .45

    sam "Ha ha, I'll keep that in mind."

    show sam relaxed neutral
    sam "So... what's all this?"

    show elliot mechanicintro surprised happy
    elliot "I was about to ask the same, actually."

    graham "Didn't I tell you earlier, Ell? It's that new machine I ordered the other day!"

    graham "According to the ad, it's supposed to help with styling, exterior makeup, something about hair removal..."

    show elliot mechanicintro surprised neutral
    elliot "{i}Hair removal?{/i}"

    graham "Yeah, like animal hair! From interiors, I'd assume!"

    show elliot mechanicintro neutral neutral
    elliot "Huh, that could be kinda useful..."

    show graham neutral:
        xzoom 1

    show graham neutral with ease:
        xpos .95

    graham "I was in the middle of configuring it before you came in here, thought we could give it a spin before we open!"

    "*tap tap tap*"

    elliot "So how exactly is this supposed to work?"

    show elliot mechanicintro surprised sneer
    elliot "Can we even fit a car in here?"

    graham "Well, it might be a tad snug now, but if we can't manage this doorway we can always knock out a wall..."

    show elliot mechanicintro neutral pained
    elliot "Ugh, that's what I need in my life, knocking out drywall..."

    show graham:
        xzoom -1
    graham "Well, that's only as a last resort, I'm sure we'll figure something out!"

    show elliot mechanicintro neutral neutral
    elliot "Sure hope so."

    show graham:
        xzoom 1
    graham "Huh, can't really make heads or tails of these controls. Keeps asking for info about us, for some reason."

    "*tap tap tap*"

    graham "Body types, rough heights, weights... maybe they're safety measures so they don't do anything to us?"

    show sam scared neutral left
    sam "I've never seen anything like this before... is new tech like this normal for you guys?"

    show elliot mechanicintro neutral happy
    elliot "Well kinda... Graham's been going off about saving all his money for the latest and greatest, so I guess this is what he had in mind..."

    graham "There's these two symbols it keeps asking me to pick between, one kinda looks like a blue arrow, and the other a pink lookin screw, maybe?"

    show elliot mechanicintro neutral pained
    elliot "I think he said he bought this out of some weird magazine? I dunno where he finds half the stuff he does."

    show elliot mechanicintro neutral happy
    elliot "There's a hundred and one other places I'd like to see that money going, but as long as I'm getting paid at the end of the day it's not my problem."

    graham "Screws seem more our style, I'm sure can figure out what that means later."

    show sam scared surprised left
    sam "There's been other stuff like this?"

    # TODO: Come up with another thing Graham bought that didn't work out
    show elliot mechanicintro surprised neutral
    elliot "Oh yeah, definitely. Lots, though nothing has worked out particularly well..."

    graham "Alright... hours of operation. That seems easy enough."

    graham "\"Register current attire for off-hours?\" Hmm... yes?"

    graham "It wants a style request now, but I don't recognize any of the choices in here... I've never heard of a \"dixie\" style exterior..."

    show sam relaxed happy right
    sam "Styling sounds interesting, I've never touched that before!"

    show elliot mechanicintro neutral happy
    elliot "Neither have Graham and I, it'll be new experiences all around."

    show elliot mechanicintro neutral sneer
    elliot "Assuming it works."

    graham "Ah well, nothing ventured, nothing gained, right? If it doesn't work, we can just ask for a refund."

    show elliot mechanicintro neutral neutral
    elliot "Is there an \"Automatic\" option? Maybe the machine can choose for itself."

    graham "Good idea. Ah, here it is! And... confirm!"

    show sam relaxed surprised
    "..."

    "..."

    show sam relaxed neutral
    show graham:
        xzoom -1
    graham "Well, that was anticlimactic, I was expecting it to do something."

    sam "What would it even do? Isn't it supposed to work on cars?"

    show graham:
        xzoom 1
    graham "I dunno, I thought maybe it'd show us a little sample of what it can do, you know?"

    show elliot mechanicintro demure sneer
    elliot "What, were you expecting it to pretty us up for the big rodeo? Do our hair and makeup?"

    # TODO Have metal bars go over the exit
    # TODO sams look surprised
    show sam scared worried

    graham "Yeah, I guess not. It's not like there's anything for it to work on in here anyways."

    show elliot mechanicintro demure happy
    elliot "So, I guess the next step is figuring out how to fit a car in here right?"

    show graham:
        xzoom -1
    graham "Yeah, let's start with mine and try-"

    graham "Huh... when'd those metal bars get there?"

    show sam scared worried with ease:
        xpos .5

    sam "Uh..."

    elliot "Well that's weird..."

    show elliot mechanicintro demure surprised
    show sam surprised left
    "The pods and hoses that were lying dormant just a moment ago sprung to life, the pods opening up and the moving around... creepily."

    "Opening their doors, the pods revealed... clothes?"

    hide elliot
    hide graham
    with dissolve

    show sam with ease:
        ypos 1.1

    "Before I could turn to ask Graham what exactly he set this machine up to do, claw arms grabbed at my limbs, raising me into the air."

    sam "Wh-whoa!"

    show sam with ease:
        xpos .2

    "Moving my body over to a pod in the corner, a curtain sprung out of the ground, blocking the rest of the room from my view."

label day_1_dressing:
    scene black
    show sam mechanic relaxed worried left at center:
        yanchor 1.0 ypos 3.4 zoom 3.0
    with fade

    sam "I can't move at... all..! What the hell is it trying to do?"

    "The face of the pod began flickering, displaying text on its exterior: \"AUTO CROSS D SESSION STARTING...\"."

    sam "This computer thinks I'm a car. I'm going to die, it's going to kill me."

    sam "It's going to try to take off my head and vacuum out my guts."

    "My heart felt like it was going to burst from of my chest."

    sam "Huff... huff... keep it together..."

    "..."

    "\"START UP COMPLETE.\""

    "\"HELLO MISS. ARE YOU READY TO LOOK YOUR BEST TODAY?\""

    show sam mechanic relaxed surprised left
    sam "Wha..."

    sam "Did you say \"miss\"?"

    "\"WONDERFUL. LET US BEGIN.\""

    "The more arms snaked their way out from the interior of the pod, pulling at my clothes."

    # Gonna go at this a little fast, can add more detail later
    sam "H-hey!"

    show sam at center with ease:
        yanchor 1.0 ypos 0.9 zoom 3.0

    "Undoing my laces, they tugged off my boots, followed shortly by my socks."

    "My bare feet dangling now in the air, I felt a slight breeze."

    show sam with ease:
        yanchor 1.0 ypos 2.75 zoom 3.0

    # TODO remove boots

    "Attacking my torso, my overalls were unbuttoned and removed."

    "My shirt, underwear, gloves, and hat all came shortly after, leaving me completely naked."

    sam "Why'd it have to take off everything?!"

    # TODO remove rest of clothes

    "Before I could complain more, my work outfit was nicely folded, placed in a box, and sucked back into the pod."

    "\"ENGAGING HAIR AND SKIN CARE.\""

    show sam with ease:
        yanchor 1.0 ypos 3.5 zoom 3.0

    "A sprayer arm appeared, applying a cool substance to the entirety of my body."

    "After I was completely coated, the substance was scraped off by a second arm, immediately followed by yet another rubbing in what I could only assume was some kind of moisturizer."

    sam "Did it just... shave off my body hair?"

    "Behind my head, I could feel my hair being pulled by several brushes, separating parts of my hair and clamping something down into them."

    sam "What's it doing now?"

    "As it continued, I could feel my head getting heavier and heavier. After pressing my hair several more times, it went back to brushing."

    sam "My hair feels... longer? Like, way longer!"

    "A myriad of small brushes were brought to my face, running from my neck up to my brows."

    "Clamps pressed my eyelashes, and a waxy substance was applied to my lips."

    "After the machine appeared satisfied, the arms withdrew and another compartment opened."

    "The machine reached into it, revealing a flannel shirt, cut off jeans, socks, sneakers, a pair of briefs, and... is that a bra?"

    show sam with ease:
        yanchor 1.0 ypos 3.0 zoom 3.0

    "Reaching around my upper torso, the bra was clasped onto me, hugging my chest."

    # PUT THE TOP ON

    show sam with ease:
        yanchor 1.0 ypos 2.25 zoom 3.0

    "At my feet, the briefs smoothly slid up my legs, tightly hugging my ass and my crotch."

    sam "This is women's underwear, isn't it..."

    "The cut-off jeans followed soon after, barely fitting around my torso."

    show sam with ease:
        yanchor 1.0 ypos .8 zoom 3.0

    "A pair of socks were slipped onto my feet, followed by some tight fitting boots."

    "\"STYLING COMPLETE. PLEASE HAVE A NICE DAY.\""

    "Gingerly setting me down, the arms retreated back into the pod. The privacy curtain shot down into the ground."

label day_1_dressing_aftermath:
    scene black with fade

    # Gets booted back out of the machine.
    # There's a conveniently placed mirror and he does a full rundown of what the machine did to him.
    "What... what did this thing put me in?"

    "Is this some kind of crop top? And Cut off jeans?"

    "Near the room entrance, I spotted a mirror."

    show sam dixie folded surprised at center:
        yanchor 1.0 zoom .8 ypos 1.0

    "Walking over to it, I reeled back as I saw my new appearance."

    show sam:
        linear 1.0 ypos 0.9 zoom 2.5

    "My work boots gone, I was now wearing a set of stylish knee-length cowboy boots."

    show sam:
        yanchor 1.0 ypos 0.9 zoom 2.5

    show sam with ease:
        yanchor 1.0 ypos 1.5 zoom 2.5

    "My legs, now completely bare, felt incredibly smooth."

    "They also were significantly more sensitive. I could feel every slight gust of wind, every point where my legs touched."

    show sam with ease:
        yanchor 1.0 ypos 2.0 zoom 2.5

    "And those legs were barely covered, if all, by jeans cut off at the highest point possible... if you could even call them jeans, at this point."

    "I was surprised that there able to keep themselves together, with the tiny seam at the bottom. The coverage they provided was barely an upgrade over underwear, if that."

    show sam with ease:
        yanchor 1.0 ypos 2.5 zoom 2.5

    "My undershirt was replaced by a short sleeved scoop-neck. Over that was a plaid collared shirt, tied off at the mid-chest."

    "Underneath that was chest padding, working its hardest to make me appear to own a pair of breasts."

    show sam with ease:
        yanchor 1.0 ypos 3.0 zoom 2.5

    "Finally dragging my eyes to my face, I was greeted by that of a girls."

    "I could hardly believe I was looking at a reflection."

    show sam scared worried

    "My lips were a ruby red, with eyeshadow and eyeliner working together to make my eyes appear twice as large."

    "My hair was draped down over my shoulders, reaching my upper back, held together with a tied off handkerchief."

    show sam scared surprised:
        linear 1.0 zoom .8 ypos 1.0

    "No matter which way I looked at it, there was a girl looking back at me."

    show sam:
        zoom .8 ypos 1.0

    "This machine... {b}put me in drag!?!{/b}"

    scene bg closet
    show sam dixie scared worried:
        xpos .2 yanchor 1.0 ypos 1.3 xzoom -1
    with fade

    "This is so strange, can even I work like this?"

    show sam dixie scared surprised:
        xzoom 1

    sam "Well I'm sure Graham can turn me back, I sure he hit a wrong button or something..."

    "The two other curtains fell down, revealing what had happened to my coworkers."

    show elliot dixie demure surprised at center with dissolve:
        yanchor 1.0 ypos 1.3 xzoom -1

    "~~ Description of Elliot goes here ~~"

    show graham neutral at right with dissolve:
        yanchor 1.0 ypos 1.3 xzoom -1 zoom .925

    "Ehhhhhhh?!?"

    "~~ Description of Graham goes here ~~"

    show elliot dixie demure surprised with ease:
        xpos .4 yanchor 1.0 ypos 1.3 xzoom 1
    "Elliot walked up next to me, taking a look at himself in the mirror, the look of shock on his face gradually growing in intensity."

    show sam:
        linear .5 xpos .1

    show elliot dixie surprised pained

    elliot "Holy shit. It put us all in drag?"

    show sam:
        xpos .1

    show elliot dixie neutral pained with ease:
        xpos .5 xzoom -1
    graham "Yeah! Ain't this machine something, Sammy? I've never seen something capable of changing someone's clothes so quickly!"

    graham "Not exactly what I expected, but pretty cool, right?"

    sam "Uh, yeah, cool. How do you make it undo this?"

    graham "Surprisingly comfortable, too. Felt like I was getting a mini spa treatment. They shave your legs too, Ell?"

    show elliot dixie surprised happy
    elliot "Yeah, I think so?"

    elliot "Though given how smooth my legs are, I think they were waxed..?"

    sam "C... could you have it turn us back? It's almost time to open."

    graham "I wonder what else it can do, what other outfits are in there?"

    elliot "Graham!"

    graham "Huh?"

    elliot "The shop's opening in five minutes and we're dressed like girls tryin to pick up dudes at NASCAR!"

    graham "Ah, yes. Correct."

    graham "..."

    graham "Aw jeez! We need to undo this before we can serve customers! What kinda shop will they think we're running!?"

    # "Graham turns back to the display and started hitting buttons."
    graham "Hold on, I'll see what I can do..."

    "*tap tap tap*"

    # Pause
    show elliot dixie neutral neutral with ease:
        xpos .45 xzoom 1

    "..."

    elliot "Wow kid, that machine really did a number on you."

    show sam with ease:
        xpos .05

    sam "Huh? How so?"

    show elliot dixie surprised happy

    elliot "It took me a moment to even recognize you, I assumed you were a girl right off the bat!"

    show sam angry
    sam "N-no way! I'm manly!"

    show elliot dixie surprised happy
    elliot "Even though you're trying to look imposing, your face still screams 'girl' to me."

    elliot "Have you ever done something like this before?"

    show sam neutral
    sam "No!? Not really?"

    elliot "Hmm, interesting."

    elliot "Ah well, just a thought. I'm sure Graham will have this undone soon enough."

    show elliot dixie neutral happy with ease:
        xpos .5 xzoom -1
    elliot "...right Graham?"

    graham "Uh, yeah, about that!"

    graham "Machine doesn't want to change us back until our shift ends?"

    show elliot dixie neutral pained
    elliot "What? {b}For real?{/b}"

    graham "Seems it registered our regular work clothes as what we wear off the clock. Won't switch us back till five."

    show sam surpsied
    sam "You can't just manually trigger a change!?"

    graham "Yeaaah, there's a lock icon over the change button? Somethin' in here about saving power. Doesn't seem to want to change our clothes more than it can help."

    graham "So uh, we might be stuck like this for the day."

    graham "Certainly one way to spice up a Monday, am I right?"

    graham "Heh heh..."

    # Elliot and Sam look visibly upset

    "{cps=*.5}...{/cps}"

    "{cps=*.5}...{/cps}"

    "{cps=*.5}...{/cps}*ding-a-ling*"

    graham "Now who wants to, uh... go take care of that customer?"

    graham "Not everyone at once!"

    # both turn to sam

    elliot "Sounds like a great... learning opportunity, right?"

    sam "I literally just started! I don't know how to use the computer!"

    # Both get closer to his face
    elliot "Instructions are in the desk drawer."

    sam "Yeah but, like this?"

    # Even closer, really huddle in
    elliot "Nobody's see you around here before, they'll just assume you're a new hire."

    graham "I believe in you, kid!"

    sam "..."


label day_1_first_customer:
    # Awkward scene where Sam talks to a customer and they misgender him / flirt with him.

    # Customer comes in and asks about their car, is a little put off by Sam's outfit
    # The car is already finished so he just has to take a form of payment and give them their keys.
    # Customer says something like "thanks miss!" on the way out
    # Sam has a silent freak out over being misgendered

    scene bg lobby zoom .6
    show customer female 01:
        yanchor 1.0 ypos 1.3 xpos .45 zoom .65
    with dissolve

    woman01 "..."

    woman01 "Are they open?"

    woman01 "{size=-5}Should I come back later?{/size}"

    woman01 "{size=-10}I mean, the door {i}was{/i} unlocked...{/size}"

    show sam surprised:
        yanchor 1.0 ypos 1.3 xpos 1.4 xzoom -1

    show sam surprised:
        linear .2 xpos .8

    sam "..."

    sam "Uh, H-hello, welcome to Jack Rabbit Car Repair, how can I help you!?!"

    woman01 "Oh, hello! I'm just here to drop off my car."

    sam "Right, uh... {size=-10}How do I do that?{/size}"

    "Pushing open the drawer under the table, I was greeted with a note pad titled \"Computer Cheat Sheet\""

    "Taking it out and flipping it open, I was greeted with a page titled \"Drop offs\"."

    nvl clear

    n "Drop offs"

    n "1. Open the customer catalog"

    n "2. Search for the {s}cusome{/s} customer's name in the system"

    n "3. If {s}thei{/s} they're not in the system, flip to \"adding a new customer\". {size=-10}Also double check their phone number{/size}"

    sam "{size=-10}Got it.{/size}"

    "Waking up the computer monitor, I was greeted with a window with a light grey background taking up the entire screen."

    sam "{size=-10}Geez, this app might be older than me...{/size}"

    "*tap* tap* tap*"

    sam "What name is it under?"

    woman01 "Susan."

    sam "Ah, ok."

    "..."

    sam "{size=-10}Where the heck is the customer catalog? Am I in the right menu?{/size}"

    sam "Heh heh, sorry, this might take a minute, I'm still getting used to this system."

    "..."

    woman01 "Are you new here? I thought it was just two guys."

    sam "Yeah. It's my first day, actually!"

    "Searching through all the tabs in the main window, I finally spotted one called \"Customer Catalog\" and clicked it. A second window popped up over the current one."

    "Typing in \"Susan\", I let out a sigh of relief as the list filtered down to a single record."

    "Double clicking it brought up a third window, this time with some general information and a list of check-ins and the reasons for each."

    sam "Is \"XXX-3700\" still correct?"

    woman01 "Yes."

    n "4. Click the add check-in button, confirm the problem they want to get resolved."

    "Checking the \"Add check-in\" button brought up a fourth window, this time with a form to fill out."

    sam "So uh, what brings you in today?"

    woman01 "The temperature gauge has been getting pretty high, and the check engine light keeps coming on."

    sam "Ah, ok."

    "*tap tap tap*"

    woman01 "They let you around the cars at all?"

    sam "Oh, not quite yet, but soon! I've been training for quite a while!"

    woman01 "Oh, so you're not just a receptionist?"

    "Receptionist!?"

    sam "N-no, I'm a mechanic!"

    woman01 "Oh, wow!"

    n "5. Take their keys and mark their car as \"in shop\"{w} {size=-10}make sure you label them!{/size}"

    n "6. If they don't have someone to pick them up, offer a ride."

    sam "Keys, please?"

    "She handed me a pair of starter and trunk keys, attached to a cat key-chain."

    "Writing \"SUSAN\" onto a tag, I attached them to the keys and added them to an empty slot on a rack behind me."

    sam "Would you like a ride somewhere?"

    woman01 "Oh, no, I have my boyfriend here to pick me up."

    woman01 "Well, I hope they let you get your hands dirty soon, we could use a couple more girls in car repair."

    sam "Yeah, thanks! Have a good day!"

    show customer female 01 at offscreenleft with moveoutleft:
        yanchor 1.0 ypos 1.3 xzoom -1

    "..."

    show sam neutral with ease:
        xpos .7

    sam "Did she say girl? That's weird...{w} She thought I was the receptionist."

    show sam neutral with ease:
        xpos .75

    sam "She thought I was a new cute receptionist. {w}She didn't think I work on cars."

    show sam surprised
    sam "I'm going to die."


label day_1_shift_start:
    # Graham and Elliot examined their outfits while Sam was dealing with customer, establish reasons why they can't take em off
    # -> Very heavy duty, seem to actually be fairly heat / burn resistant, only real issue is moving around in em
    # They start working on a couple cars, figure it'll sort itself out at the end of the day.
    # Both have Sam answer customers, maybe that was a thing he was going to do anyways


    # Elliot and Graham appear

    show elliot neutral with moveinright:
        yanchor 1.0 ypos 1.3 xpos .3 xzoom -1

    show elliot neutral:
        xzoom 1

    graham "How'd it go, kid?"

    sam "Fine. She was here to drop off her car. I checked her in.{w} She thought I was a girl."

    show graham neutral with moveinright:
        yanchor 1.0 ypos 1.3 xpos .1 xzoom -1

    show graham neutral:
        xzoom 1

    graham "Ah! Wonderful!"

    elliot "What was that last part? She thought you were a girl?"

    sam "It was mortifying."

    graham "Well I mean, in that case, nobody will suspect a thing! Just another normal day, but with a new girl handling the desk."

    sam "I was not prepared for this."

    elliot "Calm down, it'll be fine."

    graham "Yeah! We'll bring in more traffic if people think you're a cute girl! I bet we'll come out way ahead!"

    elliot "{size=-5}Not helping.{/size}"

    sam "..."

    sam "I have a spare change of clothes in my car. I'm gonna go home if that's alright with you."

    elliot "Yeah, about that... {w}these clothes won't come off."

    sam "{i}What?{/i}"

    elliot "I don't really get it either, but I can't seem to take them off. That machine probably bound them to us somehow."

    elliot "So if you want to get out of that getup, you'll have to wait 'til five.{w} {size=-5}Probably{/size}."

    sam "{b}Hrk!{/b}"

    sam "My life is over.... my career is over..."

    elliot "Hey, we have to put up with this too, at least you turned out cute!"

    sam "C... ute..?"

    elliot "Listen, we only have to deal with this for the day, right?"

    sam "...right."

    elliot "And we were going to have you deal with customers anyways, right?"

    sam "Yes."

    elliot "So if you can just get through the day, then we can all get changed back at five and be back to normal by tomorrow."

    sam "So... long..."

    elliot "Uh, maybe you can think of it as a little initiation hazing!"

    elliot "An extra way to show us what you're made of, right Graham?"

    graham "If that's what it takes, sure!"

    graham "When I started at my first shop, I had to clean some real nasty toilets. Almost hurled."

    graham "But after I finished, I got all kinds of congratulations. Really started my career off with a bang."

    graham "So hey, at least this beats scrubbing toilets, right? Smells way better too."

    sam "I suppose so..."

    # Slaps sam on the back
    graham "Great! That's the sprit!"

    elliot "Graham, someone else pulling in."

    elliot "We should get to work before they see us."

    graham "Right, right."

    graham "When you're finished with this one, step into the garage and we can get you started on a couple smaller jobs."

    sam "Heh, will do."

    graham "Alright, we're gonna get to work. Good luck!"

    # elliot and graham disappear

    show elliot neutral:
        xpos .55
    with ease

    hide elliot with dissolve

    show graham neutral:
        xpos .55
    with ease

    hide graham with dissolve


    # Sam does a bunch of different poses / voices while waiting
    sam "{size=-10}Hello, welcome to Jack Rabbit Car Repair!{/size}"

    sam "{size=-10}Welcome to Jack Rabbit Car Repair.{/size}"

    sam "{size=-10}Jack Rabbit Car Repair, how can I help you?{/size}"

    "*ding-a-ling*"

    show customer male 01 with moveinright:
        yanchor 1.0 ypos 1.3 xpos .45 xzoom 1 zoom .60

    sam "!!!"

    # -- Sam is overly nervous, man vaguely flirts with him. --
    # Maybe dials up the helpfulness / cheeriness a little to much, guy asks if he's alright
    # Sam tell him he's ok, just some first day jitters,
    # Customer asks if Graham is giving him a hard time, says he can go give Graham a talking to and set him straight
    #  - Refers to Sam as some form of "Sweetie" / ""
    # Sam insists that Graham isn't here today, he's out... doing something...
    # Customer is friends with Graham, finds that odd
    # Afterwards Sam brings this up with Graham and he doesn't make much of a big deal out of it, Sam is perplexed

    sam "{b}Hello!{/b} How can I help you!?!"

    man01 "Morning. I'd like to do a drop off."

    man01 "Haven't seen your sweet face around before, did Graham finally hire some help?"

    "Turning to the computer, I traced through the steps a second time."

    sam "{size=-10}S...weet?{/size}{w} Uh, yeah, it's my first day!"

    sam "Could I get a name?"

    man01 "Bernard. Though I might be in there are Bernie... it's been a while since somebody's asked me that, ha ha!"

    sam "Ha ha, ok..."

    sam "{i}I'm getting through these screens little faster, this time!{/i}"

    "Getting to the search screen, I started typing his name: \"B-e-r-n\""

    man01 "Got a boyfriend?"

    sam "\"n-n-n-n-n\""

    sam "What!? {b}{i}No!!!{/i}{/b}"

    man01 "Wow, really? I could have sworn someone as cute as you would have gotten snatched up already, ha ha ha!"

    man01 "{size=-5}I wonder if she'd be interested in my kid...{/size}"

    "My vision started going blurry. Pressing enter, no results were coming back."

    man01 "Woah, are you ok? It looks like you're shaking!"

    man01 "Do I need to go give Graham a talking to? He makin' you feel nervous?"

    sam "No, no, it's f-fine! Just a couple first day gitters!"

    "Deleting the extra letters, I searched again, finding a \"Bernie\"."

    "Selecting it, I hit the \"check-in\" button."

    man01 "Don't worry, I go talk to him all the time! I'm sure I can have him give you a break, or sommin'!"

    show customer male 01:
        linear 1 xpos .5

    man01 "He's usually working over here..."

    show customer male 01:
        xpos .5

    sam "No! He's... out sick today!"

    show customer male 01:
        linear 1 xpos .55

    man01 "Really? That's weird, cause his car is definitely here..."

    show customer male 01:
        xpos .55

    sam "He... had to get a ride home, because he was feeling so sick!"

    man01 "..."

    man01 "Wow, that's too bad!"

    show customer male 01 with ease:
        xpos .45

    man01 "Maybe I should go pay him a vi-"

    sam "It's very contagious!"

    man01 "Ah, I see."

    "..."

    man01 "Well hopefully he'll be feeling better when the car's ready."

    sam "Heh heh, yeah, he should be back before you know it..."

    sam "So... what was the problem?"

    man01 "Oh, I need somebody to take a look at the engine, its been skipping lately."

    sam "*tap tap tap* \"Skipping lately...\"{w} Ok, you're checked in."

    "Looking down, I saw the keys were already on my desk. Picking them up, and labelled and stored them."

    show customer male 01 with ease:
        xpos .3 xzoom -1

    man01 "Have a good one, miss. If Graham gives you a hard time when he gets back just call me over!"

    sam "I'll be sure to remember that, thank you!"

    show customer male 01 at offscreenleft with moveoutleft:
        yanchor 1.0 ypos 1.3 xzoom -1

    "..."

    "..."

    sam "{i}{size=+5}{b}Do I have a boyfriend!?! What kind of question is that!?{/b}{/size}{/i}"

    sam "{i}{size=+5}{b}I don't like guys!!!{/b}{/size}{/i}{w}{i}{size=-5}I think.{/size}{/i}"

    sam "{i}{b}And I mean, even if I did...{/b}{/i}"

    sam "{i}{b}It's not like I'd date anyone while dressed like this!!!{/b}{/i}"

    sam "..."

    sam "He really wanted to go see Graham too... I hope he's ok with me lying like that."

    sam "Who knows how this guy would react if he say Graham that way?"

    sam "I'm just trying to save his friendship, I'm sure he'll understand."

    "..."

    sam "I'm still shaking with adrenaline. Maybe I should sit down for a couple minutes, take a breather..."

    "I walked over to the \"free coffee\" area in the customer section, where a carafe on a hot plate greeted me."

    sam "I wonder how long this has been sitting here..."

    "A swig left me with an experience best described as tasting bitter, dirt flavored chalk."

    sam "Bleh, too long."

    "Looking in the cabinets, I found a suspicious looking jug of creamer and a value-sized bad of coffee grounds."

    sam "Dollar Shrub? There are dollar stores that make coffee?!"

    "Dumping the coffee out, I measured out a fresh set of grounds and poured the appropriate amount of water in the maker."

    sam "Brewing coffee... I must really look like a secretary now."

    "A stream of dark brown fluid began filling the carafe."

    sam "Well, it won't be for long! I just need to get through the rest of the day..."

    sam "Just one day like this, then I won't have to worry about what I look like."

    "After the steam died down to just a few drips a second, I poured out a new cup."

    "..."

    sam "Well... it's not {i}as{/i} bad..."

    "After giving it a few moments to cool, I hazarded a sip."

    sam "...at least it sort of tastes like coffee this time, and not some kind of dirt-sludge."

    sam "..."

    sam "I should go see what those other two are up to."

    sam "Yeah, maybe they need me help! With car things! That I'm trained for!"

    show sam neutral:
        xpos .55
    with ease

    hide sam with dissolve

    scene bg garage
    show sam neutral at right:
        yanchor 1.0 ypos 1.1 zoom .7 xzoom -1
    with fade

    # enter the car shop, look around for elliot and graham, see one of them over a car and the other under it

    sam "Let's see... where are those two?"

    show elliot neutral at center:
        yanchor 1.0 ypos 1.1 zoom .7 xzoom 1
    # show graham neutral at left:
    #     yanchor 1.0 ypos 1.1 zoom .7 xzoom 1

    elliot "Careful, Careful!"

    graham "It's fine, it's fine! I've done this a million times already!"

    elliot "That doesn't mean as much as you'd think when the last few in that million that ended with driving you to the burn ward."

    elliot "You could at least give it 30 so the engine can cool."

    graham "If I don't do it now I'll forget! Besides, I'm already under here!"

    elliot "Fiiiiiine."

    "..."

    graham "Ah! Crap!"

    "..."

    elliot "You got oil on you?"

    graham "Yeah! All over this dress!"

    "..."

    elliot "Skin burning?"

    graham "Huh, actually no!"

    graham "In fact, it's falling right off! Can barely feel the heat!"

    elliot "Huh?"

    show graham neutral at left:
        yanchor 1.0 ypos 1.1 zoom .7 xzoom 1

    graham "Whatever these clothes are made of must be somethin' special!"

    graham "I haven't gotten any grease or dirt on either, even after crawling all over the floor."

    elliot "Hmm, maybe they're not so bad, after all..."

    graham "Oh! Hey, Sam! How'd that second guy treat you?"

    # Some kinda discomfort face
    sam "..."

    elliot "That bad?"

    sam "He asked if I have a boyfriend."

    elliot "Ah."

    sam "..."

    elliot "Well, do you?"

    sam "N... no!?"

    elliot "Ah, didn't want to assume or anything."

    elliot "I'm sure he didn't mean any harm of it, even if it was a bit forward."

    sam "*sigh* Yeah, I guess..."

    sam "I can't believe it's only been a couple hours, it feels like its been an eternity."

    sam "..."

    # They both have a couple comments, Graham finds it funny that Sam felt the need to cover for him like that,
    # he's good enough friends with Bernie that he doubts anything bad would come of it. Sam is unsure of how true that is.

    sam "Oh, apparently he knows Graham, said his name was Bernie? I had to make up a lie so he wouldn't come back here."

    graham "Aw, I'm touched. You didn't have to do that!"

    graham "I bet he'd get a real kick out of this! I'd never hear the end of it! Ha ha!"

    graham "Though even if he did come back here, I don't think it'd be a big deal."

    # Graham offers to let Sam have a go at one of the cars if he can bring them both into the shop area, he goes out to do that.
    # Kinda sweats about someone seeing him outside, but there isn't really anyone around

    # After the cars have been brought in, Sam gets to start doing car things

    # ~ Time transition ~

    # Those two are impressed at how well Sam is able to diagnose problems, also how detail-oriented he is about the whole process
    # Sam figures out the issue for the first car fairly quickly, exchanges a faulty part and everything starts running smoothly
    # On the second one, he has some trouble figuring it out in a way that is indicative of a flaw he has to work on over
    # the course of the story. (Either has trouble asking for help, or gets tunnel vision when troubleshooting,
    # needs to step back and look at the whole picture)
    # One of the other guys helps him through it, by the end of the day they have everything figured out.

    # Offhandedly mention that Sam had to deal with a couple more customers.
    # He tries to get through as quickly as he can to reduce contact time, overall does a good job
    # Gets praised for consistently following the instructions

label day_1_closing:

    # ~ Time transition ~

    # It is the end of the day, they've already closed the shop
    # Turned out being a lil busier than normal, today, Graham thinks it's because of Sam bein' a cutie,
    # Elliot comes up with some other reason for they'd be busy to save face for Sam
    # (We all know in our heart of hearts it was cause Sam was bein' a cutie)

    # They go back to the changing room and manage to get changed back
    # Sam is visibly relieved, Elliot is quietly relieved, Graham is just enthusiastic about the machine doing what he thought it would
    # They all depart for home

    # End day with an internal monologue from Sam about how it went & his complex conflicted feelings




    return

# This would be good for a heart-to-heart later on
# elliot "Me and Graham, there's a couple little tells."

# elliot "Alright, well there's probably a lot for Graham."

# sam "But you on the other hand... you pass with flying colors."


