init python:
    class CharacterImage(NoRollback):

        def __init__(self, character_name, size, layers, attribute_sets,
                     aliases, layer_selector, layer_shift={}, at=[],
                     **properties):

            from collections import defaultdict

            # Basics
            self.layer_selector = layer_selector
            if isinstance(at, list):
                self.at = at
            else:
                self.at = [at]

            # Create a map which maps the (lowercase if relevant) given
            # displayable to its *evaluated* displayable--wrapped in Image,
            # found with ImageReference, etc
            self.normalize_map = {}
            for layer in layers:
                if (isinstance(layer, renpy.Displayable)
                        or isinstance(layer, renpy.atl.ATLTransformBase)):
                    k = layer
                    v = layer
                elif isinstance(layer, basestring):
                    k = layer.lower()
                    if renpy.loadable(layer):
                        v = im.Image(layer)
                    else:
                        path = "characters/{}/{}.png".format(character_name,
                                                             layer)
                        if renpy.loadable(path):
                            v = im.Image(path)
                        else:
                            v = ImageReference(layer)
                else:
                    v = ImageReference(layer)
                # Store
                self.normalize_map[k] = v

            # Make the provided `attribute_sets` frozensets
            self.attribute_sets = tuple(frozenset(attribs) for attribs in
                                        attribute_sets)

            # Make the provided `aliases` a map of frozensets
            self.aliases = {alias: frozenset(attribs) for alias, attribs in
                            aliases.iteritems()}

            # Normalize the keys in layer_shift
            self.layer_shift = {self.normalize(layer): offset for layer, offset
                                in layer_shift.iteritems()}

            # Create map of the layer displayables to an ordinal
            self.layer_order = {self.normalize(layer): index for index, layer
                                in enumerate(layers)}

            # Create a map of attributes to other attributes with which they
            # conflict
            self.attr_conflict_other = defaultdict(set)
            for attr_set in self.attribute_sets:
                for attr in attr_set:
                    conflicts = self.attr_conflict_other[attr]
                    conflicts.update(attr_set)
                    conflicts.discard(attr)

            # Resolve the attributes that an alias conflicts with by mapping
            # the alias name to a set of them
            self.alias_conflict_attr = defaultdict(set)
            for alias_name, aliased_attr in self.aliases.iteritems():
                for attr_set in self.attribute_sets:
                    if len(attr_set & aliased_attr) > 0:
                        self.alias_conflict_attr[alias_name].update(attr_set)

            # Resolve which aliases conflict with each other by mapping the
            # alias name to a set of conflicting aliases
            self.alias_conflict_other = defaultdict(set)
            for alias_name in self.aliases.iterkeys():
                for other_name in self.aliases.iterkeys():
                    if (alias_name != other_name
                        and len(self.alias_conflict_attr[alias_name]
                                & self.alias_conflict_attr[other_name]) > 0):
                        self.alias_conflict_other[alias_name].add(other_name)

            # Adjust the image size and figure out out how much to grow/offset the
            # image based on the layer shifting
            self.origin_x, self.origin_y = 0, 0
            for off in self.layer_shift.itervalues():
                self.origin_x = max(abs(off[0]), self.origin_x)
                self.origin_y = max(abs(off[1]), self.origin_y)
            self.size = (size[0] + self.origin_x * 2, size[1] + self.origin_y)

            # Properties
            properties.setdefault("style", "image_placement")
            properties.setdefault("xmaximum", size[0])
            properties.setdefault("ymaximum", size[1])
            properties.setdefault("xminimum", size[0])
            properties.setdefault("yminimum", size[1])
            self.properties = properties

        def normalize(self, layer):
            """Return the real displayable associated with the layer."""
            if isinstance(layer, basestring):
                return self.normalize_map.get(layer.lower())
            return self.normalize_map.get(layer)

        def get_banned(self, attrs):
            """Return a set of attributes that are incompatible with attrs."""
            banned = {"_"}

            for attr in attrs:
                if attr in self.alias_conflict_other:
                    banned.update(self.alias_conflict_other[attr])
                else:
                    banned.update(self.attr_conflict_other[attr])

            return banned

        def _duplicate(self, args):
            # Return None if this isn't a valid combination of attributes
            attributes = set(args.args)
            banned = self.get_banned(attributes)
            if attributes & banned:
                return None

            # Expand aliases into generic attributes
            expanded = set()
            for attr in attributes:
                if attr in self.aliases:
                    # Add attributes from this alias without creating a
                    # conflict with another attribute that overrides an alias
                    # attribute
                    expanded.update(self.aliases[attr] - banned)
                else:
                    expanded.add(attr)
            expanded = frozenset(expanded)

            # Collect the layers
            layers = {}
            for selection in self.layer_selector(self, frozenset(expanded)):
                layer = None
                if isinstance(selection, list):
                    # It's a call. Figure out which argument is the layer and
                    # perform the call
                    for i, item in enumerate(selection):
                        layer = self.normalize(item)
                        if layer:
                            selection[i] = layer
                            break
                    if not layer:
                        raise Exception("No valid layers supplied by "
                                        "selection function")
                    layers[layer] = selection[0](*selection[1:])
                else:
                    layer = self.normalize(selection)
                    if not layer:
                        raise Exception("Invalid layer supplied by "
                                        "selection function: {}"
                                        .format(selection))
                    layers[layer] = layer

            # Calculate total composition offset
            ox, oy = 0, 0
            for layer in layers:
                if layer in self.layer_shift:
                    off = self.layer_shift[layer]
                    ox += off[0]
                    oy += off[1]

            # Compose image
            composed = Fixed(**self.properties)
            for layer in sorted(layers.keys(), key=lambda x: self.layer_order[x]):
                displayable = layers[layer]
                if displayable._duplicatable:
                    layer = displayable._duplicate(args)
                composed.add(Transform(displayable,
                                       xpos=self.origin_x + ox, xanchor=0,
                                       ypos=self.origin_y + oy, yanchor=0))
            # Apply at list
            for i in self.at:
                composed = i(composed)

            return Flatten(composed)

        def _list_attributes(self, tag, attributes):
            banned = self.get_banned(attributes)

            if attributes:
                if "_" in attributes:
                    return ["_"]
                else:
                    rv = []
            else:
                rv = ["_"]

            # Aliases
            rv.extend(sorted(set(self.aliases.iterkeys()) - banned))
            # Regular attributes
            for attr_set in self.attribute_sets:
                rv.extend(sorted(attr_set - banned))

            return rv

        def _choose_attributes(self, tag, required, optional):
            # Test if we're using the special "no-op" tag, `_`, and just keep
            # all the optional
            if len(required) == 1 and "_" in required:
                if optional:
                    return tuple(sorted(optional))
                return ()

            # Ensure required attributes are in a valid combination
            required = set(required)
            banned = self.get_banned(required)
            both = required & banned

            if both:
                raise Exception("The attributes for {} conflict: {}".format(tag, " ".join(both)))

            # Remove any attribute that conflicts with `required` from
            # `optional` based on special rules with aliases
            if optional:
                optional = set(optional)
                for attr in required:
                    if attr in self.aliases:
                        optional.difference_update(self.alias_conflict_attr[attr])
                        optional.difference_update(self.alias_conflict_other[attr])
                    else:
                        optional.difference_update(self.attr_conflict_other[attr])
                required.update(optional)

            # Return final
            return tuple(sorted(required))
