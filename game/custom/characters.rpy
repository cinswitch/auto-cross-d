################################################################################
## Margret

init python:
    MARGRET_EXPRESSIONS = {
        "neutral",
        "neutral_clasped",
        "read_drink",
        "read",
        "read_tea",
        "shocked",
        "smile",
        "thinking",
        "thoughtful",
    }

    def margret_layer_selector(ci, attr):
        # Base
        for a in attr:
            if a in MARGRET_EXPRESSIONS:
                yield "base " + a.replace("_", " ")

        # Transform state--ears, nose, etc
        if "tf1" in attr:
            if "read_drink" in attr:
                yield "ears two read drink"
            elif "read" in attr or "read_tea" in attr:
                yield "ears two read"
            else:
                yield "ears two neutral"
        elif "tf2" in attr:
            if "read_drink" in attr:
                yield "muzzle read drink"
                yield "ears two read drink"
            elif "read" in attr or "read_tea" in attr:
                yield "muzzle read"
                yield "ears two read"
            else:
                yield "muzzle neutral"
                yield "ears two neutral"


image margret = CharacterImage(

    # The name of the character to look up images from in the characters folder
    character_name="margret",

    # This character's composited image size.
    size=(505, 1141),

    # A list of image or displayable names in their layer order.
    layers=(
        # base
        "base neutral clasped",
        "base neutral",
        "base read drink",
        "base read",
        "base read tea",
        "base shocked",
        "base smile",
        "base thinking",
        "base thoughtful",
        # ears
        "ears one neutral",
        "ears one read drink",
        "ears one read",
        "ears two neutral",
        "ears two read drink",
        "ears two read",
        # nose
        "muzzle neutral",
        "muzzle read drink",
        "muzzle read",
    ),

    # Sets of mutually exclusive attributes. For example, you cannot wear
    # multiple outfits at the same time, so every outfit would be put in a set
    # together.
    attribute_sets=(
        # Expression
        MARGRET_EXPRESSIONS,

        # Transform state
        {"tf1", "tf2"},
    ),

    # Let's set up some aliases! Map a single attribute to a set of equivalent
    # attributes for convenience's sake
    aliases={},

    # The layer selector is a function that is called with a single argument
    # containing the set of image attributes (with the aliases expanded). It's
    # the layer selector's job to yield all of the necessary layers based on
    # the contents of the attribute set.
    layer_selector=margret_layer_selector,
)
